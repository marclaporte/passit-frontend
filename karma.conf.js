// Karma configuration file, see link for more information
// https://karma-runner.github.io/0.13/config/configuration-file.html

module.exports = function(config) {
  config.set({
    basePath: "",
    frameworks: ["jasmine", "@angular-devkit/build-angular"],
    plugins: [
      require("karma-jasmine"),
      require("karma-chrome-launcher"),
      require("karma-jasmine-html-reporter"),
      require("karma-coverage-istanbul-reporter"),
      require("karma-webdriver-launcher"),
      require("@angular-devkit/build-angular/plugins/karma")
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    customLaunchers: {
      selenium: {
        base: "WebDriver",
        config: {
          hostname: "selenium-chrome"
        },
        browserName: "chrome"
      },
      Chromium_CI: {
        base: "Chromium",
        flags: [
          "--no-sandbox",
          "--headless",
          "--disable-gpu",
          "--remote-debugging-port=9222"
        ]
      }
    },
    hostname: process.env.DOCKER ? process.env.HOSTNAME : undefined,
    files: [],
    preprocessors: {},
    mime: {
      "text/x-typescript": ["ts", "tsx"]
    },
    coverageIstanbulReporter: {
      dir: require("path").join(__dirname, "coverage"),
      reports: ["html", "lcovonly"],
      fixWebpackSourcePaths: true
    },
    angularCli: {
      environment: "dev"
    },
    reporters:
      config.angularCli && config.angularCli.codeCoverage
        ? ["progress", "coverage-istanbul"]
        : ["progress", "kjhtml"],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ["Chrome"],
    singleRun: false,

    // to stop timeout in CI
    // https://github.com/angular/angular-cli/issues/4704#issuecomment-279875163
    captureTimeout: 60 * 1000,
    browserDisconnectTimeout: 60 * 1000,
    browserDisconnectTolerance: 5,
    browserNoActivityTimeout: 60 * 10000
  });
};
