import { OnInit, Directive } from "@angular/core";
import { Store } from "@ngrx/store";
import * as fromRoot from "../app.reducers";
import * as formActions from "./reset-form.actions";

@Directive()
export class ResetFormDirective implements OnInit {
  constructor(public store: Store<fromRoot.IState>) {}

  ngOnInit() {
    this.resetForms();
  }

  resetForms() {
    this.store.dispatch(new formActions.ResetForms());
  }
}
