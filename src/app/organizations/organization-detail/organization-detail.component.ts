import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";
import { FormGroupState } from "ngrx-forms";
import { EditOrganizationFormValue } from "../organizations.reducers";

@Component({
  selector: "app-organization-detail",
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: "organization-detail.component.html",
})
export class OrganizationDetailComponent {
  @Input() form: FormGroupState<EditOrganizationFormValue>;
  @Output() onSubmit = new EventEmitter();
}
