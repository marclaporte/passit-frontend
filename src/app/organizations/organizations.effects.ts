import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { select, Store } from "@ngrx/store";
import { exhaustMap, map, withLatestFrom } from "rxjs/operators";
import * as organizationActions from "./organizations.actions";
import { State } from "./organizations.reducers";
import { selectNewOrganizationForm } from "./organizations.selectors";
import { OrganizationsService } from "./organizations.service";

@Injectable()
export class OrganizationEffects {
  createOrganization$ = createEffect(() =>
    this.actions$.pipe(
      ofType(organizationActions.createOrganization),
      withLatestFrom(this.store.pipe(select(selectNewOrganizationForm))),
      exhaustMap(([_, form]) =>
        this.organizationsService
          .createOrganization({ name: form!.value.name })
          .pipe(
            map((organization) =>
              organizationActions.createOrganizationSuccess({ organization })
            )
          )
      )
    )
  );

  loadOrganizations$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        organizationActions.getOrganizationList,
        organizationActions.createOrganizationSuccess,
        organizationActions.setActiveOrganization
      ),
      exhaustMap((action) =>
        this.organizationsService
          .listOrganizations()
          .pipe(
            map((organizations) =>
              organizationActions.retrievedOrganizationList({ organizations })
            )
          )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<State>,
    private organizationsService: OrganizationsService
  ) {}
}
