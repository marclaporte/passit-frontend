import {
  FormGroupState,
  validate,
  updateGroup,
  createFormGroupState,
  createFormStateReducerWithUpdate
} from "ngrx-forms";
import { required, minLength } from "ngrx-forms/validation";
import { ManageMfaActions, ManageMfaActionTypes } from "./manage-mfa.actions";
import { ActivateMFAStep, IGeneratedMFA } from "./manage-mfa.interfaces";

export const FORM_ID = "Enable MFA Form";

export interface IEnableMfaForm {
  verificationCode: string;
}
export interface IEnbleMfaState {
  form: FormGroupState<IEnableMfaForm>;
  generatedMFA: IGeneratedMFA | null;
  step: ActivateMFAStep;
  errorMessage: string[] | null;
}

const validateAndUpdateFormState = updateGroup<IEnableMfaForm>({
  verificationCode: validate(required, minLength(6))
});

export const initialFormState = validateAndUpdateFormState(
  createFormGroupState<IEnableMfaForm>(FORM_ID, {
    verificationCode: ""
  })
);

export const initialState: IEnbleMfaState = {
  form: initialFormState,
  generatedMFA: null,
  step: ActivateMFAStep.Start,
  errorMessage: null
};

export const formReducer = createFormStateReducerWithUpdate<IEnableMfaForm>(
  validateAndUpdateFormState
);

export function reducer(
  state = initialState,
  action: ManageMfaActions
): IEnbleMfaState {
  const form = formReducer(state.form, action);
  state = { ...state, form };

  switch (action.type) {
    case ManageMfaActionTypes.ENABLE_MFA:
    case ManageMfaActionTypes.FORWARD_STEP:
      return {
        ...state,
        step: state.step + 1
      };

    case ManageMfaActionTypes.ENABLE_MFA_SUCCESS:
      return {
        ...state,
        generatedMFA: action.payload,
        errorMessage: null
      };

    case ManageMfaActionTypes.ENABLE_MFA_FAILURE:
      return {
        ...state,
        errorMessage: action.payload
      };

    case ManageMfaActionTypes.ACTIVATE_MFA_SUCCESS:
      return {
        ...state,
        errorMessage: null
      };

    case ManageMfaActionTypes.ACTIVATE_MFA_FAILURE:
      return {
        ...state,
        errorMessage: ["Unable to verify code."]
      };

    case ManageMfaActionTypes.RESET_FORM:
      return initialState;
  }
  return state;
}

export const getForm = (state: IEnbleMfaState) => state.form;
export const getStep = (state: IEnbleMfaState) => state.step;
export const getErrorMessage = (state: IEnbleMfaState) => state.errorMessage;
export const getProvisioningURI = (state: IEnbleMfaState) =>
  state.generatedMFA ? state.generatedMFA.provisioning_uri : null;
export const getMFAId = (state: IEnbleMfaState) =>
  state.generatedMFA ? state.generatedMFA.id : null;
