import { Action } from "@ngrx/store";

export enum DeleteAccountActionTypes {
  DELETE_ACCOUNT = "[Delete] Delete Account",
  DELETE_ACCOUNT_SUCCESS = "[Delete] Delete Account Success",
  DELETE_ACCOUNT_FAILURE = "[Delete] Delete Account Failure",
  RESET_STATE = "[Reset] Action",
  RESET_ERROR_MESSAGE = "[Delete] Reset Error Message",
}

export class DeleteAccount implements Action {
  readonly type = DeleteAccountActionTypes.DELETE_ACCOUNT;
}

export class DeleteAccountSuccess implements Action {
  readonly type = DeleteAccountActionTypes.DELETE_ACCOUNT_SUCCESS;
}

export class DeleteAccountFailure implements Action {
  readonly type = DeleteAccountActionTypes.DELETE_ACCOUNT_FAILURE;
  constructor(public payload: string) {}
}

export class ResetDeleteState implements Action {
  readonly type = DeleteAccountActionTypes.RESET_STATE;
}

export class ResetErrorMessage implements Action {
  readonly type = DeleteAccountActionTypes.RESET_ERROR_MESSAGE;
}

export type DeleteAccountActions =
  | DeleteAccountFailure
  | ResetDeleteState
  | DeleteAccount
  | DeleteAccountSuccess
  | ResetErrorMessage;
