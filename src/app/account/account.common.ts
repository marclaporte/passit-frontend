import { Routes } from "@angular/router";

import { AlreadyLoggedInGuard, LoggedInGuard } from "../guards";
import { ChangePasswordContainer } from "./change-password";
import { ForgotLearnMoreContainer } from "./change-password/forgot-learn-more/forgot-learn-more.container";
import { ErrorReportingContainer } from "./error-reporting/error-reporting.container";
import { RegisterContainer } from "./register/register.container";
import { ConfirmEmailContainer } from "./confirm-email";
import { ConfirmEmailGuard } from "./confirm-email/confirm-email.guard";
import { ResetPasswordContainer } from "./reset-password/reset-password.container";
import { ManageBackupCodeContainer } from "./manage-backup-code/manage-backup-code.container";

export const routes: Routes = [
  {
    path: "change-password",
    component: ChangePasswordContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Change Account Password"
    }
  },
  {
    path: "forgot-password",
    component: ForgotLearnMoreContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Forgot Your Password?"
    }
  },
  {
    path: "change-backup-code",
    component: ManageBackupCodeContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Change Backup Code"
    }
  },
  {
    path: "error-reporting",
    component: ErrorReportingContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Error Reporting"
    }
  },
  {
    path: "register",
    component: RegisterContainer,
    canActivate: [AlreadyLoggedInGuard],
    data: {
      title: "Register",
      showNavBar: false
    }
  },
  {
    path: "confirm-email",
    component: ConfirmEmailContainer,
    canActivate: [ConfirmEmailGuard],
    data: {
      title: "Confirm Email"
    }
  },
  {
    path: "confirm-email/:code",
    component: ConfirmEmailContainer,
    data: {
      title: "Confirm Email"
    }
  },
  {
    path: "reset-password",
    component: ResetPasswordContainer,
    canActivate: [AlreadyLoggedInGuard],
    data: {
      title: "Reset Password",
      showNavBar: false
    }
  }
];
