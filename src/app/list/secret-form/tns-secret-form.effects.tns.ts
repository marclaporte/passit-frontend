import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { DeviceType } from "@nativescript/core/ui/enums";
import { Device } from "@nativescript/core";
import { Router } from "@angular/router";
import { tap, filter } from "rxjs/operators";
import {
  RemoveSecretAction,
  SecretActionTypes
} from "../../secrets/secret.actions";

export const IS_TABLET = Device.deviceType === DeviceType.Tablet;

@Injectable()
export class TNSSecretFormEffects {
  @Effect({ dispatch: false })
  removeSecret$ = this.actions$.pipe(
    ofType<RemoveSecretAction>(SecretActionTypes.REMOVE_SECRET),
    filter(action => !IS_TABLET),
    tap(() => this.router.navigate(["/list"]))
  );

  constructor(private actions$: Actions, private router: Router) {}
}
