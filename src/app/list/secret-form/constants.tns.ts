import { DeviceType } from "@nativescript/core/ui/enums";
import { Device } from "@nativescript/core";

export const IS_TABLET = Device.deviceType === DeviceType.Tablet;
