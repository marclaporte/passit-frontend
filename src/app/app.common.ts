import { Routes } from "@angular/router";
import { LoginContainer } from "./login/login.container";
import { AlreadyLoggedInGuard, LoggedInGuard } from "./guards";
import { SecretListContainer } from "./list";
import { VerifyMfaContainer } from "./login/verify-mfa/verify-mfa.container";

export const routes: Routes = [
  { path: "", redirectTo: "list", pathMatch: "full" },
  {
    path: "account/login",
    component: LoginContainer,
    canActivate: [AlreadyLoggedInGuard],
    data: {
      title: "Log In",
      showNavBar: false
    }
  },
  {
    path: "account/login/verify-mfa",
    component: VerifyMfaContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Verify MFA",
      showNavBar: false
    }
  },
  {
    path: "list",
    component: SecretListContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Password List"
    }
  },
  {
    path: "groups",
    canActivate: [LoggedInGuard],
    loadChildren: () =>
      import("./groups/groups.module").then(mod => mod.GroupsModule)
  },
  {
    path: "account",
    loadChildren: () => import('./account/account.module').then(m => m.AccountModule)
  },
  {
    path: "import",
    loadChildren: () => import('./importer/importer.module').then(m => m.ImporterModule)
  },
  {
    path: "export",
    loadChildren: () => import('./exporter/exporter.module').then(m => m.ExporterModule)
  }
];
