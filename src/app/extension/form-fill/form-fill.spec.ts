import { IFormFillRequest } from "../interfaces";

const FormGuess = `
  <form name="horribleGuess">
    <input name="derp" />
  </form>

  <form name="almost">
    <input name="derp" />
    <input name="wut" type="text"/>
    <input type="submit">
  </form>

  <form name="dis_form" id="dis_form" action="javascript:void(0)">
    <input name="username" type="text" />
    <input name="password" type="password"/>
    <input type="submit" id="the-submit">
  </form>
`;

const FormSimple = `
  <form name="form" id="form" action="javascript:void(0)">
    <input name="username" type="text" />
    <input name="password" type="password"/>
    <input type="submit" id="id-submit">
  </form>
`;

describe("FormFill", () => {
  const body = document.getElementsByTagName("body")[0];
  const loginRequest: IFormFillRequest = {
    fields: ["login", "secret"],
    origin: window.location.origin,
    login: {
      fields: {
        login: "username",
        secret: "password"
      }
    }
  };

  beforeEach(() => {
    require("./dist/inject.js");
    // Manually clean up any forms in jasmine before proceeding.
    const forms = document.getElementsByTagName("form");
    for (let index = forms.length - 1; index >= 0; index--) {
      forms[index].parentNode.removeChild(forms[index]);
    }
  });

  // Does not run reliably in CI
  xit("should find and fill form that is the most likely to be a login form", done => {
    const element = document.createElement("div");
    body.appendChild(element).innerHTML = FormGuess;

    const submitButton = document.getElementById("the-submit");
    spyOn(submitButton as HTMLElement, "click");
    (window as any).window.browserpass.fillLogin(loginRequest);
    setTimeout(() => {
      expect(submitButton!.click).toHaveBeenCalled();
      done();
    });
  });

  xit("simple test", done => {
    const element = document.createElement("div");
    body.appendChild(element).innerHTML = FormSimple;

    const submitButton = document.getElementById("id-submit");
    spyOn(submitButton as HTMLElement, "click");
    (window as any).window.browserpass.fillLogin(loginRequest);
    setTimeout(() => {
      expect(submitButton!.click).toHaveBeenCalled();
      done();
    });
  });
});
