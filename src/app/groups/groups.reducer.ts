import {
  createReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  on
} from "@ngrx/store";
import { IState } from "../app.reducers";
import {
  showCreate,
  hideCreate,
  setManaged,
  initGroups
} from "./groups.actions";
import * as fromGroupsForm from "./groups-form/groups-form.reducer";
import * as fromContacts from "./contacts/contacts.reducer";
import { saveGroupSuccess } from "./groups-form/groups-form.actions";

export interface IGroupsPageState {
  showCreate: boolean;
  groupManaged: number | null;
}

export const initialState: IGroupsPageState = {
  showCreate: false,
  groupManaged: null
};

export const groupsPageReducer = createReducer(
  initialState,
  on(initGroups, state => ({ ...initialState })),
  on(showCreate, state => ({ ...state, showCreate: true, groupManaged: null })),
  on(hideCreate, state => ({ ...state, showCreate: false })),
  on(setManaged, (state, action) => {
    if (state.groupManaged === action.id) {
      return {
        ...state,
        groupManaged: null
      };
    } else {
      return {
        ...state,
        groupManaged: action.id,
        showCreate: false
      };
    }
  }),
  on(saveGroupSuccess, state => ({ ...state, showCreate: false }))
);

interface IGroupsModuleState {
  groupsPage: IGroupsPageState;
  groupsForm: fromGroupsForm.IGroupFormState;
  contacts: fromContacts.IContactsState;
}

export const reducers: ActionReducerMap<IGroupsModuleState> = {
  groupsPage: groupsPageReducer,
  groupsForm: fromGroupsForm.reducer,
  contacts: fromContacts.reducer
};

export interface IAppState extends IState {
  groupsModule: IGroupsModuleState;
}

export const selectFeature = createFeatureSelector<
  IAppState,
  IGroupsModuleState
>("groupsModule");

export const selectGroupsPageState = createSelector(
  selectFeature,
  feature => feature.groupsPage
);
export const selectGroupsPageShowCreate = createSelector(
  selectGroupsPageState,
  state => state.showCreate
);
export const selectGroupsPageGroupManaged = createSelector(
  selectGroupsPageState,
  state => state.groupManaged
);
export const selectGroupsPageIsShowingNewForm = createSelector(
  selectGroupsPageState,
  state => state.groupManaged === null && state.showCreate === true
);

export const selectGroupsFormState = createSelector(
  selectFeature,
  feature => feature.groupsForm
);
export const selectGroupsForm = createSelector(
  selectGroupsFormState,
  fromGroupsForm.selectForm
);
export const selectGroupMembers = createSelector(
  selectGroupsFormState,
  fromGroupsForm.selectGroupMembers
);
export const selectGroupFormIsUpdating = createSelector(
  selectGroupsFormState,
  fromGroupsForm.selectIsUpdating
);
export const selectGroupFormIsUpdated = createSelector(
  selectGroupsFormState,
  fromGroupsForm.selectIsUpdated
);

export const selectContactsState = createSelector(
  selectFeature,
  feature => feature.contacts
);
export const selectAllContacts = createSelector(
  selectContactsState,
  fromContacts.selectAllContacts
);

export const selectGroupContacts = createSelector(
  selectAllContacts,
  selectGroupMembers,
  fromGroupsForm.selectGroupContacts
);
