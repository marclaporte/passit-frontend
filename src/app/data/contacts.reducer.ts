import { EntityState, createEntityAdapter } from "@ngrx/entity";
import { createReducer, Action } from "@ngrx/store";
import { IContact } from "./interfaces";

export interface IContactState extends EntityState<IContact> {}

export const adapter = createEntityAdapter<IContact>({});

export const initialState: IContactState = adapter.getInitialState({});

const contactsReducer = createReducer(initialState);

export function reducer(state: IContactState | undefined, action: Action) {
  return contactsReducer(state, action);
}

const { selectAll } = adapter.getSelectors();

export const selectAllContacts = selectAll;
