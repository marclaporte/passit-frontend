import { Component, OnInit } from "@angular/core";
import { registerElement } from "@nativescript/angular";
import { Store, select } from "@ngrx/store";
import { filter } from "rxjs/operators";
import * as Sentry from "@sentry/angular";

import { AppDataService } from "./shared/app-data/app-data.service";
import { GetConfAction } from "./get-conf/conf.actions";
import { getRavenDsn } from "./app.reducers";

registerElement("Fab", () => require("@nstudio/nativescript-floatingactionbutton").Fab);

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html"
})
export class AppComponent implements OnInit {
  constructor(
    private appDataService: AppDataService,
    private store: Store<any>
  ) {
    this.appDataService.rehydrate();
  }

  ngOnInit() {
    this.store.dispatch(new GetConfAction());
    this.store
      .pipe(
        select(getRavenDsn),
        filter(dsn => dsn !== null)
      )
      .subscribe((dsn: string) => {
          Sentry.init({
            dsn: dsn,
            autoSessionTracking: false,
          });
      });
  }
}
