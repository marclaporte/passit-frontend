// /*
// * @angular
// */
// import { ComponentFixture, TestBed } from "@angular/core/testing";
// import { By } from "@angular/platform-browser";
//
// /*
// * Passit
// */
// import { NavbarComponent } from "./navbar.component";
// import { RouterLinkStubDirective } from "../../../../testing/router-stubs";
//
// /*
// * test setup
// */
// let fixture: ComponentFixture<NavbarComponent>;
// let linkDes;
// let links;
//
// // https://angular.io/docs/ts/latest/guide/testing.html#!#router-outlet-component
//
// describe("Testing Navbar Component: ", () => {
//
//   beforeEach(() => {
//     TestBed.configureTestingModule({
//       declarations: [
//         NavbarComponent,
//         RouterLinkStubDirective
//       ]
//     });
//
//     fixture.detectChanges();
//
//     // find DebugElements with attached RouterLinkStubDirective
//     linkDes = fixture.debugElement
//       .queryAll(By.directive(RouterLinkStubDirective));
//
//     // get attached link directive instances using DebugElement injectors
//     links = linkDes
//       .map(de => de.injector.get(RouterLinkStubDirective) as RouterLinkStubDirective);
//
//     it("can get RouterLinks from template", () => {
//       expect(links.length).toBe(4, "should have 4 links");
//       expect(links[0].linkParams).toBe("/", "1st link should go to Home");
//       expect(links[1].linkParams).toBe("/register", "2nd link should go to Register");
//       expect(links[2].linkParams).toBe("/list", "3rd link should go to List");
//       expect(links[3].linkParams).toBe("/groups", "4th link should go to Groups");
//     });
//
//     it("can click Register link in template", () => {
//       const registerLinkDe = linkDes[1];
//       const registerLink = links[1];
//
//       expect(registerLink.navigatedTo).toBeNull("link should not have navigated yet");
//
//       registerLinkDe.triggerEventHandler("click", null);
//       fixture.detectChanges();
//
//       expect(registerLink.navigatedTo).toBe("/register");
//     });
//
//   });
//
// });
