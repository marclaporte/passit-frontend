export interface ILoginForm {
  email: string;
  password: string;
  rememberMe?: boolean;
  /** Only relevant to web ext. Should form show the url input? */
  showUrl: boolean;
  url: string;
}
