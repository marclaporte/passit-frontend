import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { InlineSVGModule } from "ng-inline-svg";
import { StoreModule } from "@ngrx/store";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { NgrxFormsModule } from "ngrx-forms";

import { LoginComponent } from "./login.component";
import { LoginContainer } from "./login.container";
import * as fromRoot from "../app.reducers";
import * as fromLogin from "./login.reducer";
import { RouterTestingModule } from "@angular/router/testing";
import { SharedModule } from "../shared/shared.module";
import { ProgressIndicatorModule } from "../progress-indicator/progress-indicator.module";
import { LoginWrapperComponent } from "../login/login-wrapper/login-wrapper.component";

describe("LoginComponent", () => {
  let component: LoginContainer;
  let fixture: ComponentFixture<LoginContainer>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [LoginContainer, LoginComponent, LoginWrapperComponent],
      imports: [
        InlineSVGModule.forRoot(),
        NgrxFormsModule,
        SharedModule,
        ProgressIndicatorModule,
        HttpClientTestingModule,
        RouterTestingModule,
        StoreModule.forRoot(fromRoot.reducers),
        StoreModule.forFeature("login", fromLogin.reducers)
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginContainer);
    component = fixture.componentInstance;
  });

  it("should exist", () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it("should display validation errors if the form is submitted empty", () => {
    fixture.detectChanges();
    fixture.debugElement.nativeElement.querySelector("#loginSubmit").click();
    fixture.detectChanges();
    expect(
      fixture.nativeElement.innerText.indexOf(
        "Enter your account's email address."
      ) !== -1
    ).toBe(true);
  });
});
