import {
  animate,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";

export const flyInOut = trigger("flyInOut", [
  state(
    "in",
    style({
      transform: "translateY(0) scale(0)",
      opacity: 1
    })
  ),
  transition("void => *", [
    style({
      transform: "translateY(-50%)",
      opacity: 0
    }),
    animate("0.2s ease-in-out")
  ]),
  transition("* => void", [
    animate(
      "0.2s ease-in-out",
      style({
        transform: "translateY(-50%)",
        opacity: 0
      })
    )
  ])
]);

const listItemTrue = {
  height: "*",
  padding: "15px 0",
  opacity: 1,
  backgroundColor: "#F4FCF7",
  borderTop: "1px solid transparent",
  borderBottom: "1px solid transparent",
  boxShadow:
    "inset 0 7px 8px -7px rgba(65, 55, 65, 0.3), inset 0 -7px 8px -7px rgba(65, 55, 65, 0.3)",
  zIndex: 10
};

const listItemVoidFalse = {
  backgroundColor: "transparent",
  boxShadow:
    "inset 0 0 0 0 rgba(65, 55, 65, 0.3), inset 0 0 0 0 rgba(65, 55, 65, 0.3)",
  padding: 0,
  zIndex: 0
};

const listItemVoid = Object.assign({}, listItemVoidFalse, {
  height: 0,
  opacity: 0,
  borderTop: "1px solid transparent",
  borderBottom: "1px solid transparent"
});

const listItemFalse = Object.assign({}, listItemVoidFalse, {
  borderTop: "1px solid #D0E6E6",
  borderBottom: "1px solid #D0E6E6"
});

export const animateListItem = trigger("animateListItem", [
  state("true", style(listItemTrue)),
  state("false", style(listItemFalse)),
  transition("false => true", animate("0.5s ease-in-out")),
  transition("true => false", animate("0.3s ease-in-out")),
  transition("void => true", [
    style(listItemVoid),
    animate("0.5s ease-in-out", style(listItemTrue))
  ]),
  transition("true => void", [animate("0.3s ease-in-out", style(listItemVoid))])
]);

const secretFormTrue = {
  height: "*",
  opacity: 1,
  transform: "translateY(0)",
  zIndex: 10
};

const secretFormVoid = {
  height: 0,
  opacity: 0,
  transform: "translateY(-40px)",
  zIndex: 0
};

export const animateSecretForm = trigger("animateSecretForm", [
  state("true", style(secretFormTrue)),
  transition(":enter", [
    style(secretFormVoid),
    animate("0.5s ease-in-out", style(secretFormTrue))
  ])
]);
