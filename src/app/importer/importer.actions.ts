/* tslint:disable:max-classes-per-file */
import { Action } from "@ngrx/store";
import { ImportableSecret } from "./importableSecret";

export enum ImporterActionTypes {
  SET_IMPORT_SECRETS = "[Importer] SET_IMPORT_SECRETS",
  RESET_IMPORT_SECRETS = "[Importer] RESET_IMPORT_SECRETS",
  SET_FILE_NAME = "[Importer] SET_FILE_NAME",
  RESET_FILE_NAME = "[Importer] RESET_FILE_NAME"
}

export class SetImportSecretsAction implements Action {
  readonly type = ImporterActionTypes.SET_IMPORT_SECRETS;

  constructor(public payload: ImportableSecret[]) {}
}

export class ResetImportSecretsAction implements Action {
  readonly type = ImporterActionTypes.RESET_IMPORT_SECRETS;
}

export class SetFileNameAction implements Action {
  readonly type = ImporterActionTypes.SET_FILE_NAME;

  constructor(public payload: string) {}
}

export class ResetFileNameAction implements Action {
  readonly type = ImporterActionTypes.RESET_FILE_NAME;
}

export type Actions =
  | SetImportSecretsAction
  | ResetImportSecretsAction
  | SetFileNameAction
  | ResetFileNameAction;
